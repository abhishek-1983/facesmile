//
//  ViewController.swift
//  facesmile
//
//  Created by Pat abhishek on 2021-05-27.
//

import UIKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
    let sceneView = ARSCNView()
    let smileLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        guard ARFaceTrackingConfiguration.isSupported else {
            fatalError("Device does not support face tracking")
        }
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
           if (granted) {
            DispatchQueue.main.sync {
                
                  self.setupSmileTracker()
                
              }
           } else {
              fatalError("User did not grant camera permission!")
           }
        }
     
     
    }
    func setupSmileTracker() {
        // Configure and start face tracking session
        let configuration = ARFaceTrackingConfiguration()
        configuration.isLightEstimationEnabled = true
        
        // Run ARSession and set delegate to self
        sceneView.session.run(configuration)
        sceneView.delegate = self
        
        // Add trackingView so that it will run
        view.addSubview(sceneView)
        
        // Add smileLabel to UI
        buildSmileLabel()
    }
    func buildSmileLabel() {
        smileLabel.text = "😐"
        smileLabel.font = UIFont.systemFont(ofSize: 150)
        
        view.addSubview(smileLabel)
        
        // Set constraints
        smileLabel.translatesAutoresizingMaskIntoConstraints = false
        smileLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        smileLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    func handleSmile(smileValue: CGFloat) {
        switch smileValue {
        case _ where smileValue > 0.5:
            smileLabel.text = "😁"
        case _ where smileValue > 0.3:
            smileLabel.text = "🙂"
        case _ where smileValue > 0.1:
            smileLabel.text="😡"
        default:
            smileLabel.text = "😐"
        }
    }
    
}
extension ViewController {
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        // Cast anchor as ARFaceAnchor
        guard let faceAnchor = anchor as? ARFaceAnchor else { return }
        
        // Pull left/right smile coefficents from blendShapes
        let leftMouthSmileValue = faceAnchor.blendShapes[.mouthSmileLeft] as! CGFloat
        let rightMouthSmileValue = faceAnchor.blendShapes[.mouthSmileRight] as! CGFloat

        DispatchQueue.main.async {
            // Update label for new smile value
            self.handleSmile(smileValue: (leftMouthSmileValue + rightMouthSmileValue)/2.0)
        }
    }
    
}





